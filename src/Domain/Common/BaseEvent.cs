﻿using MediatR;

namespace ApiCleanArchitecture.Domain.Common;

public abstract class BaseEvent : INotification
{
}
