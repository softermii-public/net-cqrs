﻿using Microsoft.AspNetCore.Identity;

namespace ApiCleanArchitecture.Infrastructure.Identity;

public class ApplicationUser : IdentityUser
{
}
