﻿namespace ApiCleanArchitecture.Application.Common.Interfaces;

public interface IUser
{
    string? Id { get; }
}
